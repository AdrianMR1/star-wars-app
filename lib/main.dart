import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:starwars_app/routes/routes.dart';
import 'package:starwars_app/services/auth_service.dart';
import 'package:starwars_app/services/personaje_service.dart';

import 'services/search_service.dart';

void main() => runApp(MyApp());

// ignore: use_key_in_widget_constructors
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
            
      providers: [
        ChangeNotifierProvider(create: (_)=>AuthService()),
        ChangeNotifierProvider(create: (_)=>PersonajeService()),
        ChangeNotifierProvider(create: (_) =>CharacterSearchService())
        
      ],
      child: MaterialApp(
        title: 'StarWars App',
        initialRoute: 'login' ,
        debugShowCheckedModeBanner: false,
        routes: appRoutes
      ),
    );
  }
}