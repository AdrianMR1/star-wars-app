// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import 'package:starwars_app/search/search_delegate.dart';
import 'package:starwars_app/services/auth_service.dart';

import 'package:starwars_app/widgets/lista_personajes.dart';

class ListaPage extends StatelessWidget {
  const ListaPage({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text('Lista de personajes',style: TextStyle(color: Colors.black87),),
        leading: IconButton(
          onPressed: (){
            Navigator.pushReplacementNamed(context, 'login');
            AuthService.deleteToken();
          },
         icon: Icon(Icons.exit_to_app, color: Colors.black87)),
         actions: [
           IconButton(onPressed: () => showSearch(context: context, delegate: PersonajeSearchDelegate() ), 
           icon: Icon(Icons.search_outlined,color: Colors.black87,))
         ],
      ),
    body: ListaPersonajes(),
    );
  }
}

