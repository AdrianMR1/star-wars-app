// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, sized_box_for_whitespace
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:starwars_app/helpers/mostrar_alerta.dart';

import 'package:starwars_app/services/auth_service.dart';

import 'package:starwars_app/widgets/boton.dart';
import 'package:starwars_app/widgets/custom_input.dart';
import 'package:starwars_app/widgets/labels.dart';
import 'package:starwars_app/widgets/logo.dart';


class RegistroPage extends StatelessWidget {
   const RegistroPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return  Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                Logo(titulo: 'Registro',),
                _Form(),
                Labels(ruta: 'login',titulo: '¿Ya tienes una cuenta?' ,subTitulo: 'Ingresar Ahora' ,),
                
              ],
            ),
          ),
        ),
      )
    );
  }
}



class _Form extends StatefulWidget {
 

  @override
  State<_Form> createState() => _FormState();
}

class _FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();
  final cedulaCtrl = TextEditingController();
  final nombreCtrl = TextEditingController();
  final apellidoCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {

    final authService =  Provider.of<AuthService>(context);


    return Container(
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
            CustomInput(
            icon: Icons.assignment_ind_outlined,
            placeholder: 'Cédula',
            keyboardType: TextInputType.text,
            textController: cedulaCtrl,
          ),
            CustomInput(
            icon: Icons.perm_identity,
            placeholder: 'Nombre',
            keyboardType: TextInputType.text,
            textController: nombreCtrl,
          ),
            CustomInput(
            icon: Icons.perm_identity,
            placeholder: 'Apellidos',
            keyboardType: TextInputType.text,
            textController: apellidoCtrl,
          ),
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            isPassword: true,
            textController: passCtrl,
          ),
          Boton(text: 'Crear cuenta', onPressed:authService.autenticando ? ()=>{} : () async {
               final registroOk = await authService.register(
                 cedulaCtrl.text.trim(),
                 nombreCtrl.text.trim(),
                 apellidoCtrl.text.trim(), 
                 emailCtrl.text.trim(), 
                 passCtrl.text.trim());

               if ( registroOk == true ) {
                Navigator.pushReplacementNamed(context, 'lista');
               } else {
                 mostrarAlerta(context, 'Registro incorrecto', registroOk );
               }

             })
        ],
      ),
    );
  }
}

