// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:starwars_app/models/personaje.dart';

class DetallePage extends StatelessWidget {
  const DetallePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      final Character personaje = ModalRoute.of(context).settings.arguments as Character;
    return Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Detalle de Personaje' , style: TextStyle(color: Colors.black87),),
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(onPressed: () {
         Navigator.pushReplacementNamed(context, 'lista');
        }, 
        icon:Icon(Icons.arrow_back,color: Colors.black87) 
        ),
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Center(child:Container(
          child: Column(
            children: [
              _nombre(personaje),
              
              _altura(personaje),
              
              _peso(personaje),
            
              _colorCabello(personaje),

              _colorPiel(personaje),
              
              _colorOjos(personaje),
              
              _birthDay(personaje),
              
              _genero(personaje)
            ],
          ),
        )),
      ),
    );
  }

    Widget _nombre(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person,color: Color(0xFFFEA443)),
              title: Text('Nombre:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.name),
            ),
          ],
        ),
      ),
    );
  }
    
    Widget _altura(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.attribution_outlined ,color : Color(0xFFFEA443)),
              title: Text('Altura:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text('${personaje.height} cm'),
            ),
          ],
        ),
      ),
    );
  }

    Widget _peso(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.monitor_weight ,color : Color(0xFFFEA443)),
              title: Text('Peso:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text('${personaje.mass} kg'),
            ),
          ],
        ),
      ),
    );
  }
    
    Widget _colorCabello(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.emoji_emotions ,color : Color(0xFFFEA443)),
              title: Text('Color de cabello:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.hairColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget _colorPiel(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.emoji_people ,color : Color(0xFFFEA443)),
              title: Text('Color de piel:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.skinColor),
            ),
          ],
        ),
      ),
    );
  }
    
    Widget _colorOjos(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.preview ,color : Color(0xFFFEA443)),
              title: Text('Color de ojos:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.eyeColor),
            ),
          ],
        ),
      ),
    );
  }

    Widget _birthDay(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.cake ,color : Color(0xFFFEA443)),
              title: Text('Año de nacimiento:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.birthYear),
            ),
          ],
        ),
      ),
    );
  }

    Widget _genero(Character personaje){
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // ignore: prefer_const_literals_to_create_immutables
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person_outline_rounded ,color : Color(0xFFFEA443)),
              title: Text('Genero:',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),),
              subtitle: Text(personaje.gender),
            ),
          ],
        ),
      ),
    );
  }
}