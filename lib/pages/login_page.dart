// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:starwars_app/helpers/mostrar_alerta.dart';
import 'package:starwars_app/services/auth_service.dart';
import 'package:starwars_app/widgets/boton.dart';

import 'package:starwars_app/widgets/custom_input.dart';
import 'package:starwars_app/widgets/labels.dart';
import 'package:starwars_app/widgets/logo.dart';


class LoginPage extends StatelessWidget {
   const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return  Scaffold(
      backgroundColor: Color(0xffF2F2F2),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                Logo(titulo: 'Star Wars App',),
                _Form(),
                Labels(ruta: 'registro',titulo: '¿No tienes cuenta?',subTitulo: 'Crear una cuenta ahora',),
                
              ],
            ),
          ),
        ),
      )
    );
  }
}



class _Form extends StatefulWidget {
 

  @override
  State<_Form> createState() => _FormState();
}

class _FormState extends State<_Form> {
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
  
  final authService = Provider.of<AuthService>(context);

    return Container(
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          CustomInput(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
          ),
          CustomInput(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            isPassword: true,
            textController: passCtrl,
          ),
          Boton(text: 'Ingrese', onPressed: authService.autenticando 
              ? () => {} 
              : () async {

               FocusScope.of(context).unfocus();

               final loginOk = await authService.login( emailCtrl.text.trim(), passCtrl.text.trim() );

                if ( loginOk ) {
                   Navigator.pushReplacementNamed(context, 'lista');
                } else {
                  // Mostara alerta
                   mostrarAlerta(context, 'Login incorrecto', 'Revise sus credenciales nuevamente');
                }

             },)
        ],
      ),
    );
  }
}

