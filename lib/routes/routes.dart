// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:starwars_app/pages/Loadin_page.dart';
import 'package:starwars_app/pages/detalles.dart';
import 'package:starwars_app/pages/lista_page.dart';

import 'package:starwars_app/pages/login_page.dart';
import 'package:starwars_app/pages/registro_page.dart';


final Map<String, Widget Function(BuildContext)> appRoutes = {
  'login' : (_) => LoginPage(), 
  'registro' : (_) =>RegistroPage(),
  'lista'    : (_) => ListaPage(),
  'loading' : (_) => LoadingPage(),
  'detalle' : (_) => DetallePage()
};