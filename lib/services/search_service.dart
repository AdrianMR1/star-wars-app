// ignore_for_file: prefer_final_fields, unnecessary_new

import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:starwars_app/models/personaje.dart';


class CharacterSearchService with ChangeNotifier {
  String _url = 'swapi.dev';
  int characterPage = 0;
  bool _loading = false;
  List<Character> _charactersSearch = [];

  final _charactersSearchStreamController =
      StreamController<List<Character>>.broadcast();

  Function(List<Character>) get charactersSearchSink =>
      _charactersSearchStreamController.sink.add;

  Stream<List<Character>> get charactersSearchStream =>
      _charactersSearchStreamController.stream;

  void disposeStream() {
    _charactersSearchStreamController.close();
  }

  Future<List<Character>> _processCharacters(Uri url) async {
    final resp = await http.get(url);
    final decodedData = json.decode(resp.body);
    final charact = new CharactersList.fromJsonList(decodedData['results']);
    return charact.items;
  }

  Future<List<Character>> getCharactersSearch(String query) async {
     final url = Uri.https( _url, 'api/people', {
      'search': query,
      // 'page': characterPage.toString(),
    });

    final response = await _processCharacters(url);
    _charactersSearch.addAll(response);
    charactersSearchSink(_charactersSearch);

    return response;

  }
}