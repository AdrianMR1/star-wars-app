

// ignore_for_file: avoid_unnecessary_containers, prefer_const_constructors, unused_import

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:starwars_app/models/personaje.dart';

import 'package:starwars_app/services/search_service.dart';

class PersonajeSearchDelegate extends SearchDelegate {

  @override
  String get searchFieldLabel => 'Buscar persona';

  @override
  List<Widget> buildActions(BuildContext context) {
      return [
        IconButton(
          icon: Icon( Icons.clear ),
          onPressed: () => query = '',
        )
      ];
  }
  
    @override
    Widget buildLeading(BuildContext context) {
      return IconButton(
        icon: Icon( Icons.arrow_back ),
        onPressed: () {
          close(context, null );
        },
      );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      
           final buscarPersonaje  = Provider.of<CharacterSearchService>(context, listen: false);
     
      if(query.isEmpty){ 
        return _emptyContainer();
      }

      return FutureBuilder(
        future: buscarPersonaje.getCharactersSearch(query),
        builder: (_,AsyncSnapshot<List<Character>> snapshot){ 
            if(!snapshot.hasData) return _emptyContainer();

            final personajes = snapshot.data;

            return ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: personajes.length,
              itemBuilder: (_,int index) =>_PersonajeItem(personajes[index]),
            );
        }
        );
    }

    Widget _emptyContainer() {
      return Container(
          child: Center(
            child: Icon( Icons.person, color: Colors.black38, size: 130, ),
          ),
        );
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {

      final buscarPersonaje  = Provider.of<CharacterSearchService>(context, listen: false);
     
      if(query.isEmpty){ 
        return _emptyContainer();
      }

      return FutureBuilder(
        future: buscarPersonaje.getCharactersSearch(query),
        builder: (_,AsyncSnapshot<List<Character>> snapshot){ 
            if(!snapshot.hasData) return _emptyContainer();

            final personajes = snapshot.data;

            return ListView.builder(
              itemCount: personajes.length,
              itemBuilder: (_,int index) =>_PersonajeItem(personajes[index]),
            );
       
        }
        );


  }

}


class _PersonajeItem extends StatelessWidget {
  
  final Character personajes;
   
  const _PersonajeItem(this.personajes);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Card(
          color: Color(0xFFFFFFFF) ,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
               ListTile(
                leading: Icon(Icons.person , color: Color(0xFFFEA443)),
                title: Text(personajes.name.toString()),
                subtitle: Text('altura :${personajes.height}'
                ,style: TextStyle(color: Color(0xFF5E5855)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child:  Text('Ir a detalles', style: TextStyle(color: Color(0xFFFEA443)),),
                    onPressed: () => Navigator.pushReplacementNamed(context, 'detalle',arguments: personajes),
                  ),
                   SizedBox(width: 8),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}