// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  const Labels({Key key,
   this.ruta, 
   this.titulo, 
   this.subTitulo}) 
   : super(key: key);

  final  String ruta;
  final String titulo;
  final String subTitulo;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          Text(titulo,
          style: TextStyle(color: Colors.black54,fontSize: 15,fontWeight: FontWeight.w300)),
          SizedBox(height: 10),
          GestureDetector(
            child: Text(subTitulo,style: TextStyle(color: Color(0xFFFFA001),fontSize: 18,fontWeight: FontWeight.bold)),
            onTap: (){
              Navigator.pushReplacementNamed(context, ruta);
            },
          ),
        ],
      ),
    );
  }
}