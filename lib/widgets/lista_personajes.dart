// ignore_for_file: use_key_in_widget_constructors, avoid_unnecessary_containers, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:starwars_app/models/personaje.dart';
import 'package:starwars_app/services/personaje_service.dart';


class ListaPersonajes extends StatelessWidget {
  const ListaPersonajes({Key key}) : super(key: key);
  @override

  Widget build(BuildContext context) {
    final personaje  = Provider.of<PersonajeService>(context, listen: false);
    return FutureBuilder(
        future: personaje.getCharacters(),
        builder: (_,AsyncSnapshot snapshot){ 
            if(!snapshot.hasData) return _emptyContainer();

            final personajes = snapshot.data;

            return ListView.builder(
              itemCount: personajes.length,
              itemBuilder: (_,int index) =>Carta(personajes[index]),
            );
       
        }
        );
  }

   Widget _emptyContainer() {
      return Container(
          child: Center(
            child: Icon( Icons.person, color: Colors.black38, size: 130, ),
          ),
        );
    }
}

class Carta extends StatelessWidget {
  final Character personajes;

  const Carta( this.personajes); 
 
  
  @override
  Widget build(BuildContext context) {
    
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Card(
          color: Color(0xFFFFFFFF) ,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
               ListTile(
                leading: Icon(Icons.person , color: Color(0xFFFEA443)),
                title: Text(personajes.name),
                subtitle: Text('Fecha de cumpleaños: ${personajes.birthYear}')
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    child:  Text('Ir a detalles', style: TextStyle(color: Color(0xFFFEA443)),),
                    onPressed: () => Navigator.pushReplacementNamed(context, 'detalle',arguments: personajes)
                  ),
                   SizedBox(width: 8),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}