// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Boton extends StatelessWidget {
  const Boton({Key key, 
    this.text, 
    this.onPressed}) 
  : super(key: key);
  final String text;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Color(0xFFFFA001),
            onPrimary: Colors.white,
            elevation: 2,
            shape: StadiumBorder(),
          ) ,
          onPressed: onPressed, 
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30)
            ),
            child: Center(
              child: Text(text,style: TextStyle(color: Colors.black)),
              
            ) ,

          ),
          );
  }
}