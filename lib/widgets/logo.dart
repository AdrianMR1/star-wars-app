// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';


class Logo extends StatelessWidget {
  const Logo({Key key, 
   this.titulo}) 
  : super(key: key);

  final String titulo;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 160,
        margin: EdgeInsets.only(top: 25),
        child: Column(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
          Image(image: AssetImage('assets/logo2.png')),
          SizedBox( height: 5),
          Text(titulo,style: TextStyle(fontSize: 20),)
          
          ],
        ),
      ),
    );
  }
}