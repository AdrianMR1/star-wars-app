// To parse this JSON data, do
//
//     final usuario = usuarioFromJson(jsonString);

import 'dart:convert';

Usuario usuarioFromJson(String str) => Usuario.fromJson(json.decode(str));

String usuarioToJson(Usuario data) => json.encode(data.toJson());

class Usuario {
    Usuario({
        this.cedula,
        this.nombre,
        this.apellido,
        this.email,
        this.online,
        this.uid,
    });

    String cedula;
    String nombre;
    String apellido;
    String email;
    bool online;
    String uid;

    factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        cedula: json["cedula"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        email: json["email"],
        online: json["online"],
        uid: json["uid"],
    );

    Map<String, dynamic> toJson() => {
        "cedula": cedula,
        "nombre": nombre,
        "apellido": apellido,
        "email": email,
        "online": online,
        "uid": uid,
    };
}
